import {Injectable, Inject} from '@angular/core';
import {IAbacusApiClientConfig} from './config/iabacus-api-client-config';
import {ABACUSAPI_CLIENT_CONFIG} from './config/abacus-api-client-config';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {AuthenticationProxyService} from './authentication-proxy.service';
import {BaseQuestion} from './entities/questions/base-question';
import {IAnswer} from './entities/answers/ianswer';
import {Observable} from 'rxjs';
import {AnsweredQuestion} from './entities/answers/answered-question';
import {Guid} from './types/guid';
@Injectable()
export class QuestionService {
    constructor(
        @Inject(ABACUSAPI_CLIENT_CONFIG)private config: IAbacusApiClientConfig,
        private http: Http,
        private authenticationProxyService: AuthenticationProxyService) {
    }

    askQuestion<TQuestion extends BaseQuestion, TAnswer extends IAnswer>(question: TQuestion): Observable<AnsweredQuestion<TAnswer>> {
        return this.ask<TQuestion, TAnswer>(question);
    }

    private ask<TQuestion extends BaseQuestion, TAnswer extends IAnswer>(question: TQuestion): Observable<AnsweredQuestion<TAnswer>> {
        let that = this;
        let headers = new Headers({
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        if (this.config.isInternal) {
            headers.append('x-parenta-internal', 'true');
        } else {
            let authKey = this.authenticationProxyService.sauronKey();
            headers.append('Authorization', 'Token ' + authKey);
        }

        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.generateQuestionUrl(question), question, options).map(function (res: Response) {
            let body = res.json();

            return body || { };
        }).catch(function(error: any) {
            if (that.config.errorFunc) {
                that.config.errorFunc(error);
            }
            let errMsg = (error.message) ? error.message :
                error.status ? `${error.status} - ${error.statusText}` : 'Server error';
            console.error(errMsg); // log to console instead
            return Observable.throw(errMsg);
        });
    }

    private generateQuestionUrl(question: BaseQuestion): string {
        return this.config.apiUrl
            + Guid.newGuid() + '/question/'
            + question.getRoute();
    }
}
