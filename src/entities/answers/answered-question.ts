import {IAnswer} from './ianswer';
export class AnsweredQuestion<TAnswer extends IAnswer> {
    success: boolean;
    failMessage: string;
    result: TAnswer[];
    errors: any[];
}
