export abstract class BaseQuestion {
    constructor(private route: string) {
    }

    getRoute(): string {
        return this.route;
    }
}
